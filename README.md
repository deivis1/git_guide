Git Bash Cheat Sheet
====================

***
Initial empty repository setup
--------------
Create empty repository
```
git init
```
Add remote repository
```
git remote add origin https://user@bitbucket.org/user/repo.git
```
Setup author (amends file ~/.gitconfig, in Windows C:\Users\User\.gitconfig)
```
git config --global user.mail "user@email.com"
git config --global user.name "Name Surname"
```
***
Working with remote repositories
--------------
Change remote repository url
```
git remote set-url origin https://user@bitbucket.org/user/repo.git
```
Rename remote repository
```
git remote rename old_name new_name
```
Remove remote repository
```
git remove remote_name
```
***
Cloning repository
--------------
Clone repository
```
git clone https://user@bitbucket.org/user/repo.git
```
to clone repo without creating project folder, add `.` in the end
```
git clone https://deivis1@bitbucket.org/deivis1/git_guide.git .
```
***
Checking repository information
--------------
Show working tree status (untracked, modified files etc.)
```
git status
```
Show commits
```
git log
```
Show commits one per line
```
git log --oneline
```
Show remote repository name
```
git remote
```
Show remote repository url
```
git remote -v
```
Show local branches
```
git branch
```
Show remote branches
```
git branch -r
```
Show local and remote branches
```
git branch -a
```
***
Working with staging area
-------------------------
Stage all untracked files to index (stage files)
```
git add .
```
Unstage all files (working tree remains unchanged)
```
git reset
```
alternative way to unstage all files
```
git rm . --cached
```
unstage one file
```
git reset folder1/folder2/file.txt
```
***
Committing staged changes
-------------------------
Commit changes (text editor will open commit description file)

If you exit without saving from editor, commit operation will be aborted.

Read next chapter about how to use Vim.

```
git commit
```

Commit with comment
```
git commit -m "my comment for this commit"
```
Add modified files and commit with comment.

Caution: new untracked files will not be staged, only modified ones.
```
git commit -am "comment"
```

***
Using Vim Text Editor
---------------------
Initially vim text editor always opens in `command` mode.

In `command` mode you can do operations like save, exit, search, replace, delete line. 

In `insert` and `replace` modes you edit text content.

Basic text editor commands

|Keys                |Mode               |Command                                                         |
|--------------------|-------------------|----------------------------------------------------------------|
|`Insert`            |`command`          |switch to `insert`, `replace` mode                              |
|`Esc`               |`insert` `replace` |enters `command` mode                                           |
|`:wq` `Enter`       |`command`          |save and exit                                                   |
|`:q!` `Enter`       |`command`          |exit without saving                                             |
|`0`                 |`command`          |sets cursor to beginning of line                                |
|`$`                 |`command`          |sets cursor to end of line                                      |
|`1G` or `gg`        |`command`          |go to beginning of file                                         |
|`G`                 |`command`          |go to end of file                                               |
|`5G`                |`command`          |go to 5th line                                                  |
|`yyp` or `Yp`       |`command`          |duplicate line                                                  |
|`dd`                |`command`          |delete line                                                     |
|`5G`                |`command`          |go to 5th line                                                  |
|`\updates`          |`command`          |search for word 'updates'                                       |
|                    |                   |press `n` to go to next search occurence or `N` - to previous   |
|`:help`             |`command`          |open vim help                                                   |

Pushing commits to remote repository
------------------------------------
Push to remote repository 'origin'

all commands will have same effect
```
git push
git push origin
git push origin master
```
If you amended commit or deleted branch use push command as follows
```
git push -f
```
Push all branches to remote repository
```
git push --all
```
***
Undoing staged files and commits
--------------------------------
Unstage files (working tree will not be affected)
```
git reset 
```
Remove last commit or technically reset to 1 previous commit from HEAD

(working tree will not be affected)
```
git reset HEAD~1
git push -f
```
Safe recommended way to remove last commit changes and keep the history.

This will remove last commit and create new "revert" commit undoing their changes 
```
git revert HEAD
```
Undo working tree changes after last commit
```
git reset --hard
```
Remember that all newly created untracked files will still be there. To remove them run
```
git clean -fd
```
Change last commit comments
```
git commit --amend
```
Change 3rd previous commit comments:

Step 1. Run `git rebase -i HEAD~3`

Vim will open commit list and you will see something similar to this
```
pick a49f4e4 Comment 3
pick 9d1794a Comment 2
pick 7e76a97 Comment 1

# Rebase bb18dd1..7e76a97 onto bb18dd1 (3 command(s))
#
# Commands:
# p, pick = use commit
# r, reword = use commit, but edit the commit message
# e, edit = use commit, but stop for amending
# s, squash = use commit, but meld into previous commit
# f, fixup = like "squash", but discard this commit's log message
# x, exec = run command (the rest of the line) using shell
# d, drop = remove commit
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out
```
Step 2. Press `Insert` and replace `pick` with `r` of the commit you want to edit. In our case it will be the first line.

To continue save the text with `Esc` `:wq` `Enter`.

Step 3. Vim editor will open. Now you can amend your comment. When you are done, press `Esc` `:wq` `Enter`.

Step 4. If commit was already pushed to remote, run `git push -f`